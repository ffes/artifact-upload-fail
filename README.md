artifact-upload-fail
====================

Repository to see if GitLab CI still stays "Job succeeds" if artifact upload fails.

See issue: https://gitlab.com/gitlab-org/gitlab/-/issues/23333
